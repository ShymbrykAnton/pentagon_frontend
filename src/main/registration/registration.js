import "./registration.css"

function setFormMessage(formElement, type, message) {
    const messageElement = formElement.querySelector(".form__message");

    messageElement.textContent = message;
    messageElement.classList.remove("form__message--success", "form__message--error");
    messageElement.classList.add(`form__message--${type}`);
}

function setInputError(inputElement, message) {
    inputElement.classList.add("form__input--error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = message;
}

function clearInputError(inputElement) {
    inputElement.classList.remove("form__input--error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = "";
}

document.addEventListener("DOMContentLoaded", () => {
    const loginForm = document.querySelector("#login");
    const createAccountForm = document.querySelector("#createAccount");
    const passwordRecoveryForm = document.querySelector("#recovery");

    loginForm.addEventListener("submit", e => {
        e.preventDefault();

        // Perform AJAX/Fetch login

        setFormMessage(loginForm, "error", "Invalid username/password combination");
    });

    document.querySelectorAll(".form__input").forEach(inputElement => {
        inputElement.addEventListener("blur", e => {
            if (e.target.id === "signupUsername" && e.target.value.length > 0 && e.target.value.length < 10) {
                setInputError(inputElement, "Username must be at least 10 characters in length");
            }
        });

        inputElement.addEventListener("input", e => {
            clearInputError(inputElement);
        });
    });
});

var buttonReg = document.getElementById("buttonReg");

if(buttonReg){
    buttonReg.addEventListener("click", servletRequestReg);

}

function servletRequestReg() {
    console.log("oke reg");
    const payloadReg = {

        email: document.getElementById("email").value,
        login: document.getElementById("login").value,
        password: document.getElementById("password").value,
        name: document.getElementById("name").value,
        lname: document.getElementById("lname").value,

    };

    let requestReg = new XMLHttpRequest();
    requestReg.open("POST", "http://localhost:8099/reg");
    requestReg.onreadystatechange = function () {
        if(requestReg.readyState === XMLHttpRequest.DONE) {

            switch(requestReg.status){
                case 200:
                    alert("radostnaya kartinka")
                    break;
                case 401:
                    alert("grustnaya kartinka")
                    break;
            }
        }

    };
    request.send(JSON.stringify(payloadReg));
}

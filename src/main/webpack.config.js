const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

const pages = [{
    template: './login/index.html',
    filename: 'index.html',
    chunks: ['main']
},
    {
        template: './registration/registration.html',
        filename: 'registration.html',
        chunks: ['registration']
    },

    {
        template: './forgot/forgot.html',
        filename: 'forgot.html',
        chunks: ['forgot']
    }]

module.exports = {
    mode: 'development',
    entry: {
        main: './login/login.js',
        registration: './registration/registration.js',
        forgot: './forgot/forgot.js'

    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        ...pages.map((pageConfig) => new HTMLWebpackPlugin(pageConfig)),
        new CleanWebpackPlugin()
    ],
    devServer: {
        port: 4200,
    },
    module: {
        rules: [
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(jpg|svg|gif|png)/,
                use: ['file-loader'],
            }
        ]
    }
}
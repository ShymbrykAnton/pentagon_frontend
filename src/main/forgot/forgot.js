import "./forgot.css"

function setFormMessage(formElement, type, message) {
    const messageElement = formElement.querySelector(".form__message");

    messageElement.textContent = message;
    messageElement.classList.remove("form__message--success", "form__message--error");
    messageElement.classList.add(`form__message--${type}`);
}

function setInputError(inputElement, message) {
    inputElement.classList.add("form__input--error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = message;
}

function clearInputError(inputElement) {
    inputElement.classList.remove("form__input--error");
    inputElement.parentElement.querySelector(".form__input-error-message").textContent = "";
}

document.addEventListener("DOMContentLoaded", () => {
    const loginForm = document.querySelector("#login");
    const createAccountForm = document.querySelector("#createAccount");
    const passwordRecoveryForm = document.querySelector("#recovery");

    loginForm.addEventListener("submit", e => {
        e.preventDefault();

        // Perform AJAX/Fetch login

        setFormMessage(loginForm, "error", "Invalid username/password combination");
    });

    document.querySelectorAll(".form__input").forEach(inputElement => {
        inputElement.addEventListener("blur", e => {
            if (e.target.id === "signupUsername" && e.target.value.length > 0 && e.target.value.length < 10) {
                setInputError(inputElement, "Username must be at least 10 characters in length");
            }
        });

        inputElement.addEventListener("input", e => {
            clearInputError(inputElement);
        });
    });
});

var buttonFog = document.getElementById("buttonAuth");

if (buttonFog) {
    buttonFog.addEventListener("click", servletRequestFogPass);

}

function servletRequestFogPass() {
    alert("da");
    const payloadAuth = {

        email: document.getElementById("email").value

    };

    let requestFog = new XMLHttpRequest();
    requestFog.open("POST", "http://localhost:8099/fog");
    requestFog.onreadystatechange = function () {
        if(requestFog.readyState === XMLHttpRequest.DONE) {

            switch(requestFog.status){
                case 200:
                    alert("radostnaya kartinka")
                    break;
                case 401:
                    alert("grustnaya kartinka")
                    break;
            }
        }

    };
    request.send(JSON.stringify(payloadAuth));
}
